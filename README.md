# Настройка OSPF

Был поднят стенд согласно картинке scheme1.jpg в папке screenshots.
Обязательно нужно включить форвардинг `net.ipv4.ip_forward = 1`
и отключить проверку адреса пакета `net.ipv4.conf.all.rp_filter = 2`


*  Конфигурация OSPF R1
```
vtysh
conf t
router ospf
router-id 10.0.0.1
passive-interface default
no passive interface  eth1
no passive interface  eth2
network 10.0.0.1/32 area 0
network 192.168.0.0/24 area 0
network 192.168.1.0/24 area 0
do write
```

*  Конфигурация OSPF R2
```
vtysh
conf t
router ospf
router-id 10.0.0.2
passive-interface default
no passive interface  eth1
no passive interface  eth2
network 10.0.0.2/32 area 0
network 192.168.0.0/24 area 0
network 192.168.2.0/24 area 0
do write
```

*  Конфигурация OSPF R3
```
vtysh
conf t
router ospf
router-id 10.0.0.3
passive-interface default
no passive interface  eth1
no passive interface  eth2
network 10.0.0.2/32 area 0
network 192.168.2.0/24 area 0
network 192.168.1.0/24 area 0
do write
```

# Ассиметричный роутинг
Схема и  скриншот работы находятся в папке screenshots 
`scheme2.jpg`
`ассиметричная маршрутизация.jpg`


*  на R1 устанавливаем cost 100 на eth1

```
vtysh 
conf t
interface eth1
ip ospf cost 100
do write
```


*  на R2  устанавливаем cost 100 на eth2

```
vtysh 
conf t
interface eth2
ip ospf cost 100
do write
```
С маршрутизатора R1 выполняем команду ` ping 192.168.2.1 `
и на R2 выполняя команды` tcpdump -i eth1 tcpdump -i eth2` видим, что request приходят на eth2 а reply уходят на eth1, что означает, что трафик идет по кругу.

# Дорогой маршрут, но симметричный роутинг.

Схема и  скриншот работы находятся в папке screenshots 
`scheme3.jpg`
`дорогой маршрут.jpg`

*  на R2 устанавливаем cost 10 на eth2
```
vtysh 
conf t
interface eth2
ip ospf cost 10
do write
```

*  на R3 устанавливаем cost 1000 на eth2
```
vtysh 
conf t
interface eth2
ip ospf cost 1000
do write
```
С маршрутизатора R1 выполняем команду ` ping 192.168.2.1 ` и не смотря на то, что eth1 дорогой для R1, он все равно его использует, 
при выполнении команды `tcpdump -i eth1` на R2, видимо что reply уходят туда, откуда пришли request.
